import sys


def f_round(number: float) -> int:
    """
    Функция реализует округление вниз.

    :param number: число для округления
    :return number: округлённое число
    """
    if number % 1 > 0.5:
        number = int(number) + 1
    else:
        number = int(number)
    return number


def darken(color: str, percent: int) -> str:
    """
    Функция реализует затемнение цвета на определённый процент.
    
    Принимает цвет вида RRGGBB;
    Создаёт список из трёх каналов цвета, переведённых в целые числа;
    Каждый канал проходит процедуру затемнения, путём вычитания из числа
    его округлённой процентной части;
    Значения затемнённых цветов приводятся к виду RRGGBB.

    :param color: цвет для затемнения вида RRGGBB
    :param percent: процент затемнения
    :return color: затемнённый цвет вида RRGGBB
    """
    color_nums = [int(color[i] + color[i+1], base=16)
                  for i in range(0, 6, 2)]
    for color_index, color_num in enumerate(color_nums):
        color_nums[color_index] = color_num - f_round(color_num*(percent/100))
    color = ''
    for j in [str(hex(i)[2:]) for i in color_nums]:
        if int(j, base=16) < 16:
            color += '0' + j.upper()
        else:
            color += j.upper()
    return color


def lighten(color: str, percent: int) -> str:
    """
    Функция реализует осветление цвета на определённый процент.
    
    Принимает цвет вида RRGGBB;
    Создаёт список из трёх каналов цвета, переведённых в целые числа;
    Каждый канал проходит процедуру осветления, путём сложения числа и
    его округлённой процентной части;
    Значения осветлённых цветов приводятся к виду RRGGBB.

    Если целочисленное значение канала цвета равно 0,
    осветлённое значение находится путём умножения
    максимального значения канала (255) на процент.

    :param color: цвет для осветления
    :param percent: процент осветления
    :return color: осветлённый цвет
    """
    color_nums = [int(color[i] + color[i+1], base=16)
                  for i in range(0, 6, 2)]
    for color_index, color_num in enumerate(color_nums):
        if not color_num:
            color_nums[color_index] = f_round(255*(percent/100))
        else:
            color_nums[color_index] = color_num + f_round(color_num*(percent/100))
        if color_nums[color_index] > 255:
            color_nums[color_index] = 255
    color = ''
    for j in [str(hex(i)[2:]) for i in color_nums]:
        if int(j, base=16) < 16:
            color += '0' + j.upper()
        else:
            color += j.upper()
    return color


def main():
    """Функция запускает интерфейс для работы с функциями модуля."""
    from PySide6.QtWidgets import QApplication
    from user_interface import MainWindow
    app = QApplication([])
    window = MainWindow()
    window.setWindowTitle('Darken & Lighten')
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
