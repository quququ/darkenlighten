import re
import sys

from PySide6.QtWidgets import (QLineEdit, QPushButton,
                               QApplication, QVBoxLayout,
                               QDialog, QLabel)

from DL import darken, lighten


class MainWindow(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.label_color = QLabel('Цвет:')
        self.label_percent = QLabel('Процент:')
        self.label_result = QLabel('Результат:')
        self.text_result = QLabel('\n')
        self.edit_color = QLineEdit()
        self.edit_percent = QLineEdit()
        self.button = QPushButton('Запуск')
        layout = QVBoxLayout()
        layout.addWidget(self.label_color)
        layout.addWidget(self.edit_color)
        layout.addWidget(self.label_percent)
        layout.addWidget(self.edit_percent)
        layout.addWidget(self.label_result)
        layout.addWidget(self.text_result)
        layout.addWidget(self.button)
        self.setLayout(layout)
        self.button.clicked.connect(self.output)

    def output(self):
        if not re.fullmatch('#[0-9a-f]{6}', self.edit_color.text().lower()) or not \
                self.edit_percent.text().isdigit() or not \
                0 <= int(self.edit_percent.text()) <= 100:
            result = 'Введены некорректные данные'
        else:
            color = self.edit_color.text()
            percent = int(self.edit_percent.text())
            darken_color = darken(color[1:], percent)
            lighten_color = lighten(color[1:], percent)
            result = f'Затемнённый цвет: {darken_color}' + \
                     '\n' + f'Осветлённый цвет: {lighten_color}'
        self.text_result.setText(result)


if __name__ == '__main__':
    app = QApplication([])
    window = MainWindow()
    window.setWindowTitle('Darken & Lighten')
    window.show()
    sys.exit(app.exec_())
