import pytest
from DL import f_round, lighten, darken, main


@pytest.mark.parametrize('number,expected', [(127.5, 127),
                                             (33.3, 33),
                                             (129.94, 130),
                                             ])
def test_f_round(number, expected):
    assert f_round(number) == expected


@pytest.mark.parametrize('color,percent,expected',
                         [('000000', 100, 'FFFFFF'), ('000000', 30, '4C4C4C'),
                          ('FFFFFF', 10, 'FFFFFF'), ('FFFFFF', 50, 'FFFFFF'),
                          ('99CCFF', 50, 'E5FFFF'), ('45A1F0', 20, '53C1FF'),
                          ('00EA10', 70, 'B2FF1B'), ('AAAAAA', 0, 'AAAAAA'),
                          ])
def test_lighten(color, percent, expected):
    assert lighten(color, percent) == expected


@pytest.mark.parametrize('color,percent,expected',
                         [('000000', 100, '000000'), ('000000', 30, '000000'),
                          ('FFFFFF', 10, 'E6E6E6'), ('FFFFFF', 50, '808080'),
                          ('99CCFF', 50, '4D6680'), ('45A1F0', 20, '3781C0'),
                          ('00EA10', 70, '004605'), ('AAAAAA', 0, 'AAAAAA'),
                          ])
def test_darken(color, percent, expected):
    assert darken(color, percent) == expected
